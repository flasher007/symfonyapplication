-- phpMyAdmin SQL Dump
-- version 4.0.10.6
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1:3306
-- Generation Time: Feb 11, 2016 at 05:49 
-- Server version: 5.5.41-log
-- PHP Version: 5.5.19

SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `symfony2`
--
CREATE DATABASE IF NOT EXISTS `symfony2` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE symfony2;

-- --------------------------------------------------------

--
-- Table structure for table `actor`
--

CREATE TABLE IF NOT EXISTS `actor` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) TYPE=InnoDB AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `author`
--

CREATE TABLE IF NOT EXISTS `author` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) TYPE=InnoDB AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `book`
--

CREATE TABLE IF NOT EXISTS `book` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `description` text,
  `isbn` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) TYPE=InnoDB AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `book_author`
--

CREATE TABLE IF NOT EXISTS `book_author` (
  `book_id` int(11) NOT NULL,
  `author_id` int(11) NOT NULL,
  PRIMARY KEY (`book_id`,`author_id`),
  UNIQUE KEY `UNIQ_9478D345F675F31B` (`author_id`),
  KEY `IDX_9478D34516A2B381` (`book_id`)
) TYPE=InnoDB;

-- --------------------------------------------------------

--
-- Table structure for table `book_tag`
--

CREATE TABLE IF NOT EXISTS `book_tag` (
  `book_id` int(11) NOT NULL,
  `tag_id` int(11) NOT NULL,
  PRIMARY KEY (`book_id`,`tag_id`),
  UNIQUE KEY `UNIQ_F2F4CE15BAD26311` (`tag_id`),
  KEY `IDX_F2F4CE1516A2B381` (`book_id`)
) TYPE=InnoDB;

-- --------------------------------------------------------

--
-- Table structure for table `Film`
--

CREATE TABLE IF NOT EXISTS `Film` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `description` text,
  `quality` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) TYPE=InnoDB AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `film_actor`
--

CREATE TABLE IF NOT EXISTS `film_actor` (
  `film_id` int(11) NOT NULL,
  `actor_id` int(11) NOT NULL,
  PRIMARY KEY (`film_id`,`actor_id`),
  UNIQUE KEY `UNIQ_DD19A8A910DAF24A` (`actor_id`),
  KEY `IDX_DD19A8A9567F5183` (`film_id`)
) TYPE=InnoDB;

-- --------------------------------------------------------

--
-- Table structure for table `film_tag`
--

CREATE TABLE IF NOT EXISTS `film_tag` (
  `film_id` int(11) NOT NULL,
  `tag_id` int(11) NOT NULL,
  PRIMARY KEY (`film_id`,`tag_id`),
  UNIQUE KEY `UNIQ_1CBA72DBBAD26311` (`tag_id`),
  KEY `IDX_1CBA72DB567F5183` (`film_id`)
) TYPE=InnoDB;

-- --------------------------------------------------------

--
-- Table structure for table `Tag`
--

CREATE TABLE IF NOT EXISTS `Tag` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) TYPE=InnoDB AUTO_INCREMENT=1 ;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `book_author`
--
ALTER TABLE `book_author`
  ADD CONSTRAINT `FK_9478D345F675F31B` FOREIGN KEY (`author_id`) REFERENCES `author` (`id`),
  ADD CONSTRAINT `FK_9478D34516A2B381` FOREIGN KEY (`book_id`) REFERENCES `book` (`id`);

--
-- Constraints for table `book_tag`
--
ALTER TABLE `book_tag`
  ADD CONSTRAINT `FK_F2F4CE15BAD26311` FOREIGN KEY (`tag_id`) REFERENCES `Tag` (`id`),
  ADD CONSTRAINT `FK_F2F4CE1516A2B381` FOREIGN KEY (`book_id`) REFERENCES `Book` (`id`);

--
-- Constraints for table `film_actor`
--
ALTER TABLE `film_actor`
  ADD CONSTRAINT `FK_DD19A8A910DAF24A` FOREIGN KEY (`actor_id`) REFERENCES `actor` (`id`),
  ADD CONSTRAINT `FK_DD19A8A9567F5183` FOREIGN KEY (`film_id`) REFERENCES `Film` (`id`);

--
-- Constraints for table `film_tag`
--
ALTER TABLE `film_tag`
  ADD CONSTRAINT `FK_1CBA72DBBAD26311` FOREIGN KEY (`tag_id`) REFERENCES `Tag` (`id`),
  ADD CONSTRAINT `FK_1CBA72DB567F5183` FOREIGN KEY (`film_id`) REFERENCES `Film` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
