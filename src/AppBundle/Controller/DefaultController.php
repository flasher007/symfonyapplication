<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{

    public function indexAction(Request $request)
    {

        $em = $this->getDoctrine()->getManager();

        $books = $em->getRepository('AppBundle:Book')->findBy([], ['title' => 'ASC'], 5);
        $films = $em->getRepository('AppBundle:Film')->findBy([], ['title' => 'ASC'], 5);

        return $this->render('default/index.html.twig', [
            'books' => $books,
            'films' => $films,
            'locale' => $request->getLocale()
        ]);
    }

}
