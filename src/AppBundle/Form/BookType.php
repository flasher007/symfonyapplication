<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class BookType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', null, ['label' => 'Название книги'])
            ->add('description', null, ['label' => 'Описание'])
            ->add('isbn')
            ->add('authors', null, ['label' => 'Авторы'])
            ->add('tags', EntityType::class, [
                'class' => 'AppBundle:Tag',
                'multiple' => 'true',
                'choice_label' => 'getName',
                'label' => 'Теги',
                'attr' => ['class' => 'tag-list']
            ])
        ;
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Book'
        ));
    }
}
