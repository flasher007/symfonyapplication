<?php

namespace AppBundle\Services;

use Knp\Menu\FactoryInterface;
use Symfony\Component\HttpFoundation\RequestStack;

class MenuBuilder
{
    private $factory;

    /**
     * @param FactoryInterface $factory
     */
    public function __construct(FactoryInterface $factory) {
        $this->factory = $factory;
    }

    public function createMainMenu(RequestStack $requestStack) {
        $menu = $this->factory->createItem('root');

        $menu->addChild('Главная', array('route' => 'homepage'));
        $menu->addChild('Книги', array('route' => 'book_index'));
        $menu->addChild('Фильмы', array('route' => 'film_index'));

        return $menu;
    }
}