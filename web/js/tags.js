var $tagContainer = $('.tag-list'),
    $addTagLink = $('.add-tag'),
    $newTag = $('.new-tag');

jQuery(document).ready(function() {

    $addTagLink.on('click', function(e) {
        var $self = $(this),
            href = $self.data('href');
        e.preventDefault();
        if ($newTag.val().length) {
            addTagForm($tagContainer, $newTag, href);
        } else {
            $newTag.parent('div').addClass('has-error');
        }
        return false;
    });
});

function addTagForm($tagContainer, $newTag, href) {
    var newTagValue = $newTag.val();

    $.ajax({
        url: href,
        method: 'post',
        data: $newTag.serialize(),
        dataType: "json",
        success: function(response) {
            $tagContainer.append($('<option>', {
                value: response.id,
                text: response.text
            }));
            $tagContainer.val(response.id);
        }
    });

}